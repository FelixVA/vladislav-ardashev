-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 11, 2024 at 03:37 AM
-- Server version: 8.0.30
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Eshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `OurOrder`
--

CREATE TABLE `OurOrder` (
  `Id of order` int NOT NULL,
  `Id buyer` int NOT NULL,
  `Id Product` int NOT NULL,
  `Quantity Prod` int NOT NULL,
  `Sum of price` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `OurOrder`
--

INSERT INTO `OurOrder` (`Id of order`, `Id buyer`, `Id Product`, `Quantity Prod`, `Sum of price`) VALUES
(202, 1, 10101, 1, 200);

-- --------------------------------------------------------

--
-- Table structure for table `Product`
--

CREATE TABLE `Product` (
  `Id` int NOT NULL,
  `Name of prod` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Quantity` int NOT NULL,
  `Price` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Product`
--

INSERT INTO `Product` (`Id`, `Name of prod`, `Quantity`, `Price`) VALUES
(10101, 'Darek', 5, 200);

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `Id` int NOT NULL,
  `Name` char(25) NOT NULL,
  `Pass` char(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`Id`, `Name`, `Pass`) VALUES
(1, 'Vladislav Ardashev', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `OurOrder`
--
ALTER TABLE `OurOrder`
  ADD PRIMARY KEY (`Id of order`),
  ADD UNIQUE KEY `Id buyer` (`Id buyer`,`Id Product`),
  ADD KEY `Id Product` (`Id Product`);

--
-- Indexes for table `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`Id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `OurOrder`
--
ALTER TABLE `OurOrder`
  ADD CONSTRAINT `ourorder_ibfk_1` FOREIGN KEY (`Id Product`) REFERENCES `Product` (`Id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `ourorder_ibfk_2` FOREIGN KEY (`Id buyer`) REFERENCES `User` (`Id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
